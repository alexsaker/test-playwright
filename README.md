# Playwright

## Prerequisites
The only dependencies needed are jest a nd playwright.
```command
 docker build -t asaker/playwright:bionic -f docker/Dockerfile .
 docker push asaker/playwright:bionic
```

## Installation
```code
npm i
```
## Launch test
```code
npm run test
```

## Resources
https://playwright.dev/