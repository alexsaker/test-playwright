const { chromium } = require('playwright');
const expect = require('expect');
let browser;
let page;
beforeAll(async () => {
  browser = await chromium.launch({ headless: true });
});
afterAll(async () => {
  await browser.close();
});
beforeEach(async () => {
  page = await browser.newPage();
});
afterEach(async () => {
  await page.close();
});
it('should work', async () => {
  await page.goto('https://www.example.com/');
  await page.screenshot({ path: `snapshots/example.png` });
  expect(await page.title()).toBe('Example Domain');
});
